﻿using System;

namespace SharpLzo
{
    public class LzoException : Exception
    {
        public LzoResult Result { get; }

        public LzoException(LzoResult result)
        {
            Result = result;
        }

        public LzoException(LzoResult result, string message)
            : base(message)
        {
            Result = result;
        }
    }
}
