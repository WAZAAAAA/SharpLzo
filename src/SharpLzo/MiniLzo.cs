﻿using System;
using SharpLzo.Native;

namespace SharpLzo
{
    public class MiniLzo
    {
        private readonly byte[] _workMemory;

        public static MiniLzo Instance { get; } = new MiniLzo();
        public static uint Version => LzoNative.Instance.Version();

        static MiniLzo()
        {
            var result = LzoNative.Instance.Init();
            if (result != LzoResult.OK)
                throw new LzoException(result, $"Failed to initialize lzo library result=0x{result:X8}");
        }

        public MiniLzo()
            : this(new byte[65536])
        {
        }

        public MiniLzo(byte[] workMemory)
        {
            _workMemory = workMemory;
        }

        public byte[] Compress(byte[] data)
        {
            var compressed = Compress(data, out var result);
            if (result != LzoResult.OK)
                throw new LzoException(result);

            return compressed;
        }

        public byte[] Compress(byte[] data, out LzoResult result)
        {
            var compressedLength = data.Length + data.Length / 64 + 16 + 3 + 4;
            var compressed = new byte[compressedLength];
            result = LzoNative.Instance.Compress(data, data.Length, compressed, ref compressedLength, _workMemory);
            if (compressedLength != compressed.Length)
                Array.Resize(ref compressed, compressedLength);

            return compressed;
        }

        public byte[] Decompress(byte[] data)
        {
            return Decompress(data, data.Length * 10);
        }

        public byte[] Decompress(byte[] data, int decompressedLength)
        {
            var decompressed = Decompress(data, decompressedLength, out var result);
            if (result != LzoResult.OK)
                throw new LzoException(result);

            return decompressed;
        }

        public byte[] Decompress(byte[] data, int decompressedLength, out LzoResult result)
        {
            var decompressed = new byte[decompressedLength];
            result = LzoNative.Instance.DecompressSafe(data, data.Length, decompressed, ref decompressedLength, _workMemory);
            if (decompressedLength != decompressed.Length)
                Array.Resize(ref decompressed, decompressedLength);

            return decompressed;
        }
    }
}
