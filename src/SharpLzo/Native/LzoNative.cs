﻿using System;
using System.Runtime.InteropServices;

namespace SharpLzo.Native
{
    internal abstract class LzoNative
    {
        public static LzoNative Instance { get; }

        static LzoNative()
        {
            LzoNative native = null;
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                native = new LzoNativeLinux();
            }

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                switch (RuntimeInformation.ProcessArchitecture)
                {
                    case Architecture.X64:
                        native = new LzoNative64Bit();
                        break;

                    case Architecture.X86:
                        native = new LzoNative32Bit();
                        break;

                    default:
                        throw new PlatformNotSupportedException("SharpLzo only supports win32, win64 and linux x64");
                }
            }

            try
            {
                native.Version();
                Instance = native;
            }
            catch (DllNotFoundException)
            {
                throw new DllNotFoundException("Unable to find native MiniLzo library");
            }
        }

        public abstract LzoResult Init();
        public abstract uint Version();
        public abstract LzoResult Compress(byte[] inData, int inLength, byte[] outData, ref int outLength, byte[] wrkmem);
        public abstract LzoResult DecompressSafe(byte[] inData, int inLength, byte[] outData, ref int outLength, byte[] wrkmem);
    }
}
